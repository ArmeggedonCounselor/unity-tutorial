﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour {

	public Transform tf;

	public float speed = 0f;

	// Use this for initialization
	void Start () {
		tf = this.gameObject.GetComponent<Transform> ();
	}
	
	// Update is called once per frame
	void Update () {
		// Check if K is pressed
		if (Input.GetKeyDown (KeyCode.K)) {
			// if K is pressed, move the gameObject "speed" units up-left.
			tf.position += (Vector3.up + Vector3.left) * speed;
		}
		// Check if Space is pressed.
		if (Input.GetKeyDown (KeyCode.Space)) {
			// if Space is pressed, move the gameObject to (0,0,0)
			tf.position = Vector3.zero;
		}
	}
}
